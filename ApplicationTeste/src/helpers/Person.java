/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

/**
 *
 * @author Guilherme
 */
public class Person {
    private String ID;
    private String firstName;
    private String lastName;
    private String timeStamp;
    
    public Person(String ID, String firstName, String lastName, String timeStamp){
        this.ID = ID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.timeStamp = timeStamp;
    }
    public String getID(){
        return this.ID;
    }
    public String getFirstName(){
        return this.firstName;
    }
    public String getLastName(){
        return this.lastName;
    }
    public String getTimeStamp(){
        return this.timeStamp;
    }
}

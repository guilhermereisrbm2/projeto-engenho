/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user.view;

/**
 *
 * @author Guilherme
 */
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import user.Main;

public class LoginViewController {
    
    private Main main;
    
    @FXML
    private TextField username;
    
    @FXML
    private TextField password;
    
    @FXML
    private void executeLogin() throws IOException{
        main.executeLogin(username.getText(),password.getText());
    }
}

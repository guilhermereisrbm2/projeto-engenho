/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user.view;

import helpers.Person;
import java.sql.SQLException;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import user.Main;

/**
 *
 * @author Guilherme
 */
public class TableViewController {
    
    //main class
    private Main main;
    
    //fields

    @FXML
    private TableView tableview;
    
    private ObservableList<ObservableList> data;



    //methods
    @FXML
    private void initialize() throws SQLException{
        System.out.println("FOI FOI FOI FOOOOOOOOOOIIIIII");
        
        
        data = FXCollections.observableArrayList();
        
        /**********************************
         * TABLE COLUMN ADDED DYNAMICALLY *
         **********************************/
        for(int i=0 ; i<main.resultSet.getMetaData().getColumnCount(); i++){
            //We are using non property style for making dynamic table
            final int j = i;                
            TableColumn col = new TableColumn(main.resultSet.getMetaData().getColumnName(i+1));
            col.setCellValueFactory(new Callback<CellDataFeatures<ObservableList,String>,ObservableValue<String>>(){                    
                public ObservableValue<String> call(CellDataFeatures<ObservableList, String> param) {                                                                                              
                    return new SimpleStringProperty(param.getValue().get(j).toString());                        
                }                    
            });

            tableview.getColumns().addAll(col); 
            System.out.println("Column ["+i+"] ");
        }
        
        /********************************
         * Data added to ObservableList *
         ********************************/
        while(main.resultSet.next()){
            //Iterate Row
            ObservableList<String> row = FXCollections.observableArrayList();
            for(int i=1 ; i<=main.resultSet.getMetaData().getColumnCount(); i++){
                //Iterate Column
                row.add(main.resultSet.getString(i));
            }
            System.out.println("Row [1] added "+row );
            data.add(row);
        }
        
        
        
        //FINALLY ADDED TO TableView
        tableview.setItems(data);
        tableview.refresh();
        
        /*
        while(main.resultSet.next()){
            data.add(
                new Person(
             main.resultSet.getString(0),
             main.resultSet.getString(1),
             main.resultSet.getString(2),
             main.resultSet.getString(3))
            );
        }
        table.getColumns().get(0).setVisible(false);
        table.getColumns().get(0).setVisible(true);
        */
        
        /**
        maritalStatusBox.setValue("Single");
        maritalStatusBox.setItems(maritalStatusList);
        
        mainDepartmentBox.setValue("Electrical");
        mainDepartmentBox.setItems(mainDepartmentList);
        
        departmentBox.setValue("Design");
        departmentBox.setItems(electricalList);
        **/
        
    }
}

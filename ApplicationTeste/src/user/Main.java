/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user;

import connection.NetworkSingleton;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 *
 * @author Guilherme
 */
public class Main extends Application {
    
    //public vars
    public static ResultSet resultSet = null;
    
    //private vars
    private static PreparedStatement pstmt = null;
    
    private static Stage primaryStage;
    private static BorderPane mainLayout;
    
    @Override
    public void start(Stage primaryStage) throws IOException {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Application Teste");
        showMainView();
        showLoginView();
        //showTableView();
    }
    
    private void showMainView() throws IOException{
        FXMLLoader loader = new FXMLLoader();
        URL url = Main.class.getResource("view/MainView.fxml");
        loader.setLocation(url);
        mainLayout = loader.load();
        Scene scene = new Scene(mainLayout,800,600);
        primaryStage.setScene(scene);
        primaryStage.show();    
    }
    
    
    
    public void showLoginView() throws IOException{
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("view/LoginView.fxml"));
        HBox loginView = loader.load();
        mainLayout.setCenter(loginView);
    }
    public static void showTableView() throws IOException{
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("view/TableView.fxml"));
        AnchorPane tableView = loader.load();
        mainLayout.setCenter(tableView);
    }
       
    public static void main(String[] args) {
        launch(args);
    }

    public static void executeLogin(String username, String password) throws IOException {
        try {
            String query = "SELECT * FROM users WHERE Fname = ? AND Lname = ?";
            Connection conn = NetworkSingleton.getConnection();
            if (conn.isClosed()) {
                System.out.println("IT'S CLOSED !");
            } else {

                pstmt = conn.prepareStatement(query);
                pstmt.setString(1, username);
                pstmt.setString(2, password);
                resultSet = pstmt.executeQuery();

                //if there's anything in the result set
                if(resultSet.next()){
                    System.out.println("Login Successful !");
                    
                    String queryAll = "SELECT * FROM users";
                    pstmt = conn.prepareStatement(queryAll);
                    resultSet = pstmt.executeQuery();
                    
                    showTableView();
                } else {
                    System.out.println("That username and password combination doesn't exist.");
                }
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}

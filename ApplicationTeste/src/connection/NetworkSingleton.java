package connection;

import com.mysql.jdbc.Statement;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Connection;

/***
To use the JDBC driver, call Class.forName() to load it before creating the connection with
DriverManager.getConnection() in your code.

JDBC uses a connection string in the following format:

jdbc:driver://hostname:port/dbName?user=userName&password=password

You can retrieve the hostname, port, database name, user name, and password from the environment 
variables that Elastic Beanstalk provides to your application. The driver name is specific to 
your database type and driver version. The following are example driver names:
*/
public class NetworkSingleton {

    
    private static final String USERNAME = "guilhermereisrbm";
    private static final String PASSWORD = "holycrap";
    private static final String CONNSTRING = "jdbc:mysql://localhost:8889/youTube";
    

    //"jdbc:driver://hostname:port/dbName?user=userName&password=password"
    private static final String AMAZON_CONNSTRING = "jdbc:mysql://mysqldbinstance.cac2crtdxs5x.us-west-2.rds.amazonaws.com:3306/masterdb";
    
    public static Connection conn = null;
        
    public static Connection getConnection() {
        
        
        if (conn != null){
            return conn;
        }
        
        Statement stmt = null;
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(AMAZON_CONNSTRING, USERNAME, PASSWORD);
            System.out.println("Network.java | Connected !");
   
            
        } catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
         }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
         }
        
        return conn;
    }
    
}
